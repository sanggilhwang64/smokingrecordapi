package com.sanggil.smokingrecordapi.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
