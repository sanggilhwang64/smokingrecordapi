package com.sanggil.smokingrecordapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmokingRecordApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(SmokingRecordApiApplication.class, args);
    }

}
