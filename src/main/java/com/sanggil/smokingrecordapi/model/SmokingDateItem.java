package com.sanggil.smokingrecordapi.model;

import com.sanggil.smokingrecordapi.entity.Smoking;
import com.sanggil.smokingrecordapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.format.DateTimeFormatter;


@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class SmokingDateItem {
    @ApiModelProperty(notes = "오늘 핀 갯수")
    private Integer todayNumber;

    @ApiModelProperty(notes = "흡연 날짜")
    private String smokingDate;


    private SmokingDateItem(Builder builder) {
        this.todayNumber = builder.todayNumber;
        this.smokingDate = builder.smokingDate;
    }

    public static class Builder implements CommonModelBuilder<SmokingDateItem> {
        private final Integer todayNumber;
        private final String smokingDate;

        public Builder(Smoking smoking) {
            String smokingDateText = smoking.getSmokingDate().format(DateTimeFormatter.ofPattern("yyyy년 MM월 dd일 E요일"));

            this.todayNumber = smoking.getTodayNumber();
            this.smokingDate = smokingDateText;
        }

        @Override
        public SmokingDateItem build() {
            return new SmokingDateItem(this);
        }
    }
}
