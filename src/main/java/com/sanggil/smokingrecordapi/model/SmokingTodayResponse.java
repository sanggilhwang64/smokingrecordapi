package com.sanggil.smokingrecordapi.model;

import com.sanggil.smokingrecordapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor
public class SmokingTodayResponse {
    @ApiModelProperty(notes = "오늘 핀 갯수")
    private Integer todayNumber;

    @ApiModelProperty(notes = "오늘 금액")
    private Long todayPrice;

    @ApiModelProperty(notes = "최근 7일 평균 갯수")
    private Integer averageSevenDaysNumber;

    @ApiModelProperty(notes = "마지막 흡연 시간")
    private LocalDateTime lastSmokingTime;

    private SmokingTodayResponse(Builder builder) {
        this.todayNumber = builder.todayNumber;
        this.todayPrice = builder.todayPrice;
        this.averageSevenDaysNumber = builder.averageSevenDaysNumber;
        this.lastSmokingTime = builder.lastSmokingTime;
    }
    public static class Builder implements CommonModelBuilder<SmokingTodayResponse>{
        private final Integer todayNumber;
        private final Long todayPrice;
        private final Integer averageSevenDaysNumber;
        private final LocalDateTime lastSmokingTime;

        public Builder(int todayNumber, long todayPrice, int averageSevenDaysNumber, LocalDateTime lastSmokingTime) {
            this.todayNumber = todayNumber;
            this.todayPrice = todayPrice;
            this.averageSevenDaysNumber = averageSevenDaysNumber;
            this.lastSmokingTime = lastSmokingTime;
        }
        @Override
        public SmokingTodayResponse build() {
            return new SmokingTodayResponse(this);
        }
    }
}
