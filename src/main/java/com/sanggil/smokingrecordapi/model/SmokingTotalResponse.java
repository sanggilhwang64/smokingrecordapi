package com.sanggil.smokingrecordapi.model;

import com.sanggil.smokingrecordapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class SmokingTotalResponse {

    @ApiModelProperty(notes = "최근 7일 갯수")
    private Integer near7DaysNumber;

    @ApiModelProperty(notes = "최근 7일 금액")
    private Long near7DaysPrice;

    @ApiModelProperty(notes = "최근 30일 갯수")
    private Integer near30DaysNumber;

    @ApiModelProperty(notes = "최근 30일 금액")
    private Long near30DaysPrice;

    @ApiModelProperty(notes = "총 갯수")
    private Integer allNumber;

    @ApiModelProperty(notes = "총 금액")
    private Long allPrice;

    private SmokingTotalResponse(Builder builder) {
        this.near7DaysNumber = builder.near7DaysNumber;
        this.near7DaysPrice = builder.near7DaysPrice;
        this.near30DaysNumber = builder.near30DaysNumber;
        this.near30DaysPrice = builder.near30DaysPrice;
        this.allNumber = builder.allNumber;
        this.allPrice = builder.allPrice;
    }

    public static class Builder implements CommonModelBuilder<SmokingTotalResponse> {
        private final Integer near7DaysNumber;
        private final Long near7DaysPrice;
        private final Integer near30DaysNumber;
        private final Long near30DaysPrice;
        private final Integer allNumber;
        private final Long allPrice;

        public Builder(int near7DaysNumber, long near7DaysPrice, int near30DaysNumber, long near30DaysPrice, int allNumber, long allPrice) {
            this.near7DaysNumber = near7DaysNumber;
            this.near7DaysPrice = near7DaysPrice;
            this.near30DaysNumber = near30DaysNumber;
            this.near30DaysPrice = near30DaysPrice;
            this.allNumber = allNumber;
            this.allPrice = allPrice;

        }

        @Override
        public SmokingTotalResponse build() {
            return new SmokingTotalResponse(this);
        }
    }
}
