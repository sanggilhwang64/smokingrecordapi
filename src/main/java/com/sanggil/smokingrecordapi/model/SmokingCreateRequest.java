package com.sanggil.smokingrecordapi.model;

import com.sanggil.smokingrecordapi.enums.CigaretteType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class SmokingCreateRequest {

    @ApiModelProperty(notes = "담배 종류")
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private CigaretteType cigaretteType;

    @ApiModelProperty(notes = "니코틴 함량")
    @NotNull
    private Double nicotineContent;

    @ApiModelProperty(notes = "타르 함량")
    @NotNull
    private Integer tarContent;

    @ApiModelProperty(notes = "가격")
    @NotNull
    private Long price;
}
