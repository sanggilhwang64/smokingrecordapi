package com.sanggil.smokingrecordapi.controller;

import com.sanggil.smokingrecordapi.model.*;
import com.sanggil.smokingrecordapi.service.ResponseService;
import com.sanggil.smokingrecordapi.service.SmokingService;
import com.sanggil.smokingrecordapi.service.StatisticsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "흡연 기록 관리")
@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/smoking")
public class SmokingController {
    private final SmokingService smokingService;
    private final StatisticsService statisticsService;

    @ApiOperation(value = "담배 정보 등록")
    @PostMapping("/new")
    public CommonResult setSmoking(@RequestBody @Valid SmokingCreateRequest createRequest) {
        smokingService.setSmoking(createRequest);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "날짜별 흡연 기록")
    @GetMapping("/date")
    public ListResult<SmokingDateItem> getSmokingDate() {
        return ResponseService.getListResult(smokingService.getSmokingDate(),true);
    }

    @ApiOperation(value = "오늘의 흡연 기록")
    @GetMapping("/today")
    public SingleResult<SmokingTodayResponse> getData() {
        return ResponseService.getSingleResult(statisticsService.getData());
    }

    @ApiOperation(value = "흡연 기록 통계")
    @GetMapping("/total")
    public SingleResult<SmokingTotalResponse> getAllData() {
        return ResponseService.getSingleResult(statisticsService.getAllData());
    }


    @ApiOperation(value = "흡연 추가")
    @PutMapping("/add/{id}")
    public CommonResult putSmoking(@PathVariable long id) {
        smokingService.putSmoking(id);

        return ResponseService.getSuccessResult();
    }

}
