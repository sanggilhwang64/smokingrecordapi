package com.sanggil.smokingrecordapi.repository;

import com.sanggil.smokingrecordapi.entity.Smoking;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface SmokingRepository extends JpaRepository<Smoking, Long> {
    List<Smoking> findAllBySmokingDateBetween(LocalDate endDate, LocalDate startDate);
    List<Smoking> findTop1ByOrderByLastSmokingTimeDesc();
}
