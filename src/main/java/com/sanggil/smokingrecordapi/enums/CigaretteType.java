package com.sanggil.smokingrecordapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CigaretteType {
    PARLIMENT_AQUA3("아쿠아 3"),
    PARLIMENT_AQUA5("아쿠아 5")
    ;

    private final String cigaretteName;
}
