package com.sanggil.smokingrecordapi.entity;

import com.sanggil.smokingrecordapi.enums.CigaretteType;
import com.sanggil.smokingrecordapi.interfaces.CommonModelBuilder;
import com.sanggil.smokingrecordapi.model.SmokingCreateRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Smoking {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "담배 종류")
    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 20)
    private CigaretteType cigaretteType;

    @ApiModelProperty(notes = "니코틴 함량")
    @Column(nullable = false)
    private Double nicotineContent;

    @ApiModelProperty(notes = "타르 함량")
    @Column(nullable = false)
    private Integer tarContent;

    @ApiModelProperty(notes = "가격")
    @Column(nullable = false)
    private Long price;

    @ApiModelProperty(notes = "오늘 핀 갯수")
    @Column(nullable = false)
    private Integer todayNumber;

    @ApiModelProperty(notes = "흡연 날짜")
    private LocalDate smokingDate;

    @ApiModelProperty(notes = "마지막 흡연 시간")
    private LocalDateTime lastSmokingTime;

    private Smoking(Builder builder) {
        this.cigaretteType = builder.cigaretteType;
        this.nicotineContent = builder.nicotineContent;
        this.tarContent = builder.tarContent;
        this.price = builder.price;
        this.todayNumber = builder.todayNumber;
        this.smokingDate = builder.smokingDate;
        this.lastSmokingTime = builder.lastSmokingTime;
    }
    public void putSmoking() {
        this.todayNumber += 1;
        this.lastSmokingTime = LocalDateTime.now();
        this.smokingDate = LocalDate.now();
    }
    public static class Builder implements CommonModelBuilder<Smoking> {
        private final CigaretteType cigaretteType;
        private final Double nicotineContent;
        private final Integer tarContent;
        private final Long price;
        private final Integer todayNumber;
        private final LocalDate smokingDate;
        private final LocalDateTime lastSmokingTime;
        public Builder(SmokingCreateRequest request) {
            this.cigaretteType = request.getCigaretteType();
            this.nicotineContent = request.getNicotineContent();
            this.tarContent = request.getTarContent();
            this.price = request.getPrice();
            this.todayNumber = 0;
            this.smokingDate = null;
            this.lastSmokingTime = null;
        }
        @Override
        public Smoking build() {
            return new Smoking(this);
        }
    }
}
