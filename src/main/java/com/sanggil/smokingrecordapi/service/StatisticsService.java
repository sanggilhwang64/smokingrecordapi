package com.sanggil.smokingrecordapi.service;

import com.sanggil.smokingrecordapi.entity.Smoking;
import com.sanggil.smokingrecordapi.model.SmokingTodayResponse;
import com.sanggil.smokingrecordapi.model.SmokingTotalResponse;
import com.sanggil.smokingrecordapi.repository.SmokingRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class StatisticsService {
    private final SmokingRepository smokingRepository;

    public SmokingTodayResponse getData() {
        LocalDate today = LocalDate.now();
        LocalDate minus7Day = today.minusDays(7);
        List<Smoking> near7DayList =
                smokingRepository.findAllBySmokingDateBetween(minus7Day, today);
        List<Smoking> lastSmoking =
                smokingRepository.findTop1ByOrderByLastSmokingTimeDesc();

        int averageSevenDaysNumber = 0;
        LocalDateTime lastSmokingTime = lastSmoking.get(0).getLastSmokingTime();
        int todayNumber = 0;
        long todayPrice = 0;

        for (Smoking item : near7DayList) {
            averageSevenDaysNumber += item.getTodayNumber() / 7;
            todayNumber = item.getTodayNumber();
            todayPrice = item.getTodayNumber() * 225L;
        }
        return new SmokingTodayResponse.Builder(
                todayNumber,
                todayPrice,
                averageSevenDaysNumber,
                lastSmokingTime).build();

    }

    public SmokingTotalResponse getAllData() {
        LocalDate today = LocalDate.now();
        LocalDate minus7Day = today.minusDays(7);
        LocalDate minus30Day = today.minusDays(30);
        List<Smoking> near7DayList = smokingRepository.findAllBySmokingDateBetween(minus7Day, today);
        List<Smoking> near30DayList = smokingRepository.findAllBySmokingDateBetween(minus30Day, today);
        List<Smoking> allList = smokingRepository.findAll();

        int near7DaysNumber = 0;
        long near7DaysPrice = 0;
        int near30DaysNumber = 0;
        long near30DaysPrice = 0;
        int allNumber = 0;
        long allPrice = 0;

        for (Smoking item1 : near7DayList) {
            near7DaysNumber += item1.getTodayNumber();
            near7DaysPrice += near7DaysNumber * 225L;
        }
        for (Smoking item2 : near30DayList) {
            near30DaysNumber += item2.getTodayNumber();
            near30DaysPrice += near30DaysNumber * 225L;
        }
        for (Smoking item3 : allList) {
            allNumber += item3.getTodayNumber();
            allPrice += allNumber * 225L;
        }
        return new SmokingTotalResponse.Builder(
                near7DaysNumber,
                near7DaysPrice,
                near30DaysNumber,
                near30DaysPrice,
                allNumber,
                allPrice
        ).build();

    }
}
