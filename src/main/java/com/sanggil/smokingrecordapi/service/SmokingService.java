package com.sanggil.smokingrecordapi.service;

import com.sanggil.smokingrecordapi.entity.Smoking;
import com.sanggil.smokingrecordapi.exception.CMissingDataException;
import com.sanggil.smokingrecordapi.model.ListResult;
import com.sanggil.smokingrecordapi.model.SmokingCreateRequest;
import com.sanggil.smokingrecordapi.model.SmokingDateItem;
import com.sanggil.smokingrecordapi.repository.SmokingRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SmokingService {
    private final SmokingRepository smokingRepository;

    public void setSmoking(SmokingCreateRequest request) {
        Smoking addData = new Smoking.Builder(request).build();

        smokingRepository.save(addData);
    }

    public ListResult<SmokingDateItem> getSmokingDate() {
        List<Smoking> originList = smokingRepository.findAll();

        List<SmokingDateItem> result = new LinkedList<>();

        originList.forEach(item -> result.add(new SmokingDateItem.Builder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public void putSmoking(long id) {
        Smoking originData = smokingRepository.findById(id).orElseThrow(CMissingDataException::new);

        originData.putSmoking();

        smokingRepository.save(originData);
    }
}
